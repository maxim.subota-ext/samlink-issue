package com.enfuce;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;

public class Main {
    // Provide your subscription key here !!!
    private static final String API_KEY = "PROVIDE YOUR KEY HERE";
    private static final int THREAD_NUMBER = 32;
    private static final CountDownLatch cdl = new CountDownLatch(THREAD_NUMBER);

    public static void main(String[] args) {

        Runnable request = Main::sendRequest;

        for (int i = 0; i < THREAD_NUMBER; i++) {
            new Thread(request).start();
        }
    }


    private static void sendRequest() {
        HttpClient httpClient = HttpClient.newHttpClient();

        String body = "{\n" +
                "  \"access\": {\n" +
                "    \"accounts\": [],\n" +
                "    \"balances\": [],\n" +
                "    \"transactions\": []\n" +
                "  },\n" +
                "  \"combinedServiceIndicator\": false,\n" +
                "  \"frequencyPerDay\": 4,\n" +
                "  \"recurringIndicator\": true,\n" +
                "  \"validUntil\": \"2020-03-30\"\n" +
                "}";

        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("https://sml-prod-psd2.azure-api.net/psd2/v1/consents"))
                .header("Content-Type", "application/json")
                .header("X-Request-ID", UUID.randomUUID().toString())
                .header("Ocp-Apim-Subscription-Key", API_KEY)
                .POST(HttpRequest.BodyPublishers.ofString(body))
                .build();


        HttpResponse response = null;
        try {
            cdl.countDown();
            cdl.await();
            System.out.println("Thread " + Thread.currentThread().getId() + " is sending request.");
            response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (response != null && response.statusCode() == 201) {
            System.out.println("Thread " + Thread.currentThread().getId() + " has got success result.");
        } else {
            System.out.println(response.body());
        }
    }
}
